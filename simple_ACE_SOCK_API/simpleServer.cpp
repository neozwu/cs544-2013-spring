#include <iostream>
#include <string>
#include <sstream>

#include "ace/INET_Addr.h"
#include "ace/SOCK_Stream.h"
#include "ace/SOCK_Acceptor.h"
#include "ace/SOCK_Connector.h"
#include "ace/Log_Msg.h"

int main (void) {

    typedef ACE_SOCK_Acceptor ACCEPTOR;
    ACCEPTOR::PEER_ADDR server_addr (50000);
    ACCEPTOR acceptor;

    if (acceptor.open(server_addr) == -1 ) return 1;

    for (ACCEPTOR::PEER_STREAM peer;;) {
        if (acceptor.accept (peer) == -1 ) return 1;

        char buf[4096];
        ssize_t bytes_received;

        while ( (bytes_received =
                    peer.recv(buf,sizeof(buf))) > 0 ) {

            peer.send_n(buf,bytes_received);
            std::cout << "wainting for receving more data" << std::endl;
            std::cout << "bytes_received is: " << bytes_received << std::endl;

        }
    std::cout << "wainting for another reqeust" << std::endl;

    }

}

