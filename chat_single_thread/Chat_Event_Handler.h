#ifndef CHAT_EVENT_HANDLER_H
#define CHAT_EVENT_HANDLER_H

#include "headers.h"

class Chat_Event_Handler : public ACE_Event_Handler {
protected:
    ACE_SOCK_Stream stream_;
    //ACE_SOCK_Acceptor::PEER_STREAM stream_;

public:
    Chat_Event_Handler (ACE_Reactor *r ) : ACE_Event_Handler (r) {}

    virtual ~Chat_Event_Handler() {}

    virtual int open ();

    ACE_SOCK_Stream &peer () { return stream_; }

    virtual int get_handle() const { return stream_.get_handle();}

    virtual int handle_input (ACE_HANDLE);

    virtual int handle_close (ACE_HANDLE, ACE_Reactor_Mask);

};


#endif

