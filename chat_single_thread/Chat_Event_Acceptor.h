#ifndef CHAT_EVENT_ACCEPTOR_H
#define CHAT_EVENT_ACCEPTOR_H

class Chat_Event_Acceptor: public ACE_Event_Handler {

private:
    ACE_SOCK_Acceptor acceptor_;

public:
    typedef ACE_INET_Addr PEER_ADDR;

    Chat_Event_Acceptor (ACE_Reactor *r = ACE_Reactor:: instance() )
        : ACE_Event_Handler(r) {}

    virtual int open (const ACE_INET_Addr &local_addr ) {
        acceptor_.open(local_addr);
        return reactor () ->register_handler ( this,
                                            ACE_Event_Handler::ACCEPT_MASK);

    }

    virtual int handle_close (ACE_HANDLE, ACE_Reactor_Mask ) {
        //std::cout << "accept handle close called!" << std::endl;
        acceptor_.close ();
        delete this;
        return 0;
    }

    virtual int get_handle () const { return acceptor_.get_handle (); }

    virtual int handle_input (ACE_HANDLE);

protected:
    virtual ~Chat_Event_Acceptor() {}

};

#endif
