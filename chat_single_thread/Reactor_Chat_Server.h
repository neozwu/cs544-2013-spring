#ifndef REACTOR_CHAT_SERVER_H 
#define REACTOR_CHAT_SERVER_H 

#include "headers.h"
#include "Chat_Event_Acceptor.h"

template <typename ACCEPTOR>
class Reactor_Chat_Server : public ACCEPTOR {

public:
    Reactor_Chat_Server ( int argc, char * argv[] , ACE_Reactor * reactor);

};

template <typename ACCEPTOR>
Reactor_Chat_Server<ACCEPTOR>::Reactor_Chat_Server (
            int argc, char * argv[], ACE_Reactor *reactor )
    : ACCEPTOR (reactor) {
    
    u_short Chat_port = argc > 1 ? atoi ( argv[1] ) : 50000;

    typename ACCEPTOR::PEER_ADDR server_addr (Chat_port, INADDR_ANY);

    if (ACCEPTOR::open (server_addr) == -1 )
        reactor->end_reactor_event_loop ();

}


#endif

