#include "headers.h"
#include "Chat_Event_Acceptor.h"
#include "Chat_Event_Handler.h"

int Chat_Event_Acceptor::handle_input (ACE_HANDLE) {

    //std::cout << "handling connecton request!" << std::endl;

    Chat_Event_Handler *peer_handler = 0;

    ACE_NEW_RETURN ( peer_handler,
                     Chat_Event_Handler (reactor ()),-1);

    acceptor_.accept (peer_handler -> peer ());

    //std::cout << "now activate the event_handler" << std::endl;

    peer_handler->open ();

    //return -1; //this will trigger handle_close(), just for debug
    
    return 0;

}

