#include <iostream>
#include <string>
#include "ace/streams.h"
#include "ace/Reactor.h"
#include "Reactor_Chat_Server.h"
#include "Chat_Event_Acceptor.h"


typedef Reactor_Chat_Server<Chat_Event_Acceptor> Chat_Server_Daemon;

int main ( int argc, char * argv[] ) {

    ACE_Reactor reactor;

    new Chat_Server_Daemon ( argc, argv, &reactor);

    reactor.run_reactor_event_loop ();

}

