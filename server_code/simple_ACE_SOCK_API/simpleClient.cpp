#include <iostream>
#include <string>
#include <sstream>

#include "ace/INET_Addr.h"
#include "ace/SOCK_Stream.h"
#include "ace/SOCK_Acceptor.h"
#include "ace/SOCK_Connector.h"
#include "ace/Log_Msg.h"

int main ( int argc, char * argv[] ) {

    std::string server_hostname = 
        argc > 1 ? argv[1] : "129.25.9.123"; //tux ip
    const char* msg = 
        argc > 2 ? argv[2] : "hello, world";

    typedef ACE_SOCK_Connector CONNECTOR;

    CONNECTOR connector;
    CONNECTOR::PEER_STREAM peer;
    CONNECTOR::PEER_ADDR peer_addr (50000, server_hostname.c_str());

    ACE_Time_Value timeout (10);

    if ( connector.connect( peer, peer_addr, &timeout ) == -1 ) { 
        std::cerr << "connection failed!" << std::endl;
        return -1;
    }

    
    char buf[BUFSIZ];

    if (peer.send_n( msg, (strlen(msg)+1)) == -1 ) return 1;

    for (ssize_t n;
            ( n = peer.recv ( buf, sizeof buf, &timeout )) > 0; )
        ACE::write_n (ACE_STDOUT, buf, n );

    return 0;
}

