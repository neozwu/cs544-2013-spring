#include "Chat_Event_Handler.h"
#include <iostream>
#define ACE_NTRACE 0

int Chat_Event_Handler::open () {
    //std::cout << "going to open event_handler!" << std::endl;
    return reactor() -> register_handler (this, ACE_Event_Handler::READ_MASK);

}

int Chat_Event_Handler::handle_input (ACE_HANDLE) {

    //This is where the protocol handling logic should go 
    //right now, only echo whatever received

    ACE_TRACE("chat_event_handler::handle_input");

    char buffer[4096];
    ssize_t bytes_received;

    if (( bytes_received = peer().recv(buffer, sizeof(buffer))) > 0 ) {
        std::cout << "received: " << buffer << std::endl;
        peer().send_n("echo:",5);
        peer().send_n(buffer, bytes_received);
        peer().send_n("\n",1);
    } else {
        return 1;
    }

    peer ().close();

    return 0;

}

int Chat_Event_Handler::handle_close(ACE_HANDLE, ACE_Reactor_Mask) {
    stream_.close ();
    delete this;
    return 0;
}



