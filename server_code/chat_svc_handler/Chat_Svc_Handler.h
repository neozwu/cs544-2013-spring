#ifndef CHAT_SVC_HANDLER_H
#define CHAT_SVC_HANDLER_H
#include "ace/SOCK_Stream.h"
#include "ace/Svc_Handler.h"
#include <iostream>


class Chat_Svc_Handler 
    : public ACE_Svc_Handler<ACE_SOCK_Stream, ACE_NULL_SYNCH> {


public:

    int handle_input (ACE_HANDLE) {
        
        //
        //The application-related logic goes here.
        //
        //right now, just echo things back

        //std::cout << "get called here" << std::endl;
        //

        char buffer[4096];
        ssize_t bytes_received;

        if (( bytes_received = this->peer().recv(buffer, sizeof(buffer))) > 0 ) {
            this->peer().send_n("echo:",5);
            this->peer().send_n(buffer, bytes_received);
            this->peer().send_n("\n",1);
            std::cout << "received: " << buffer << std::endl;
        } else {
            return 1;
        }

        this->peer ().close();

        return 0;
    }
};


#endif

