#if defined (ACE_HAS_EXPLICIT_TEMPLATE_INSTANTIATION)
template class ACE_Acceptor<Chat_Svc_Handler, ACE_SOCK_ACCEPTOR>;
template class ACE_Svc_Handler<ACE_SOCK_STREAM, ACE_NULL_SYNCH>;
#elif defined (ACE_HAS_TEMPLATE_INSTANTIATION_PRAGMA)
#pragma instantiate ACE_Acceptor<Chat_Svc_Handler, ACE_SOCK_ACCEPTOR>
#pragma instantiate \
   ACE_Svc_Handler<ACE_SOCK_STREAM, ACE_NULL_SYNCH>
#endif /* ACE_HAS_EXPLICIT_TEMPLATE_INSTANTIATION */

#include <iostream>
#include <string>
#include <ace/ACE.h>
#include <ace/Acceptor.h>
#include <ace/SOCK_Acceptor.h>
#include "ace/streams.h"
#include "ace/Reactor.h"
#include "ace/Log_Msg.h"
#include "Chat_Svc_Handler.h"
#include "Reactor_Chat_Server.h"

typedef ACE_Acceptor<Chat_Svc_Handler, ACE_SOCK_Acceptor>  Chat_Svc_Acceptor;

typedef Reactor_Chat_Server<Chat_Svc_Acceptor>  Chat_Server_Daemon;

int main ( int argc, char * argv[] ) {

    //ACE_TRACE("main"); 

    ACE_Reactor reactor;

    new Chat_Server_Daemon ( argc, argv, &reactor);

    reactor.run_reactor_event_loop ();

}
