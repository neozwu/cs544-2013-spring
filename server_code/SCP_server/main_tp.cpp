#include <iostream>
#include <string>
#include <ace/SOCK_acceptor.h>
#include <ace/Acceptor.h>
#include <ace/Event_Handler.h>
#include <ace/Reactor.h>
#include <ace/TP_Reactor.h>
#include <ace/Thread_Manager.h>
#include "Reactor_Chat_Server.h"
#include "Chat_Svc_Handler.h"
#include "Quit_Handler.h"


static void *controller ( void *); 
static void *event_loop ( void *);

typedef ACE_Acceptor<Chat_Svc_Handler,ACE_SOCK_Acceptor> Chat_Svc_Acceptor;
typedef Reactor_Chat_Server<Chat_Svc_Acceptor> Chat_Server_Daemon;

hash_map<char*,char*> user_list;

int main ( int argc, char * argv[] ) {

    const size_t THREAD_POOL_SIZE = 4;

    ACE_TP_Reactor tp_reactor;

    ACE_Reactor reactor (&tp_reactor);

    new Chat_Server_Daemon ( argc, argv, &reactor);

    ACE_Thread_Manager::instance() -> spawn_n (
                THREAD_POOL_SIZE, event_loop, &reactor);

    ACE_Thread_Manager::instance() -> spawn ( controller, &reactor);

    return ACE_Thread_Manager::instance() -> wait ();


}


static void *event_loop (void * arg) {
    ACE_Reactor *reactor = static_cast<ACE_Reactor *> (arg);
    reactor->run_reactor_event_loop();

    return 0;
}

static void *controller (void *arg) {
    ACE_Reactor *reactor = static_cast<ACE_Reactor *> (arg);
    Quit_Handler *quit_handler = new Quit_Handler (reactor);

    for(;;) {


        std::string user_input;
        std::getline(std::cin,user_input,'\n');

        if (user_input == "quit" ) {
            reactor->notify(quit_handler);
            break;
        }
    }


}


