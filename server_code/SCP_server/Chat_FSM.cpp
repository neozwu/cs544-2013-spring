#include "ace/ACE.h"
#include "ace/FILE_Addr.h"
#include "ace/Sock_Stream.h"
#include "ace/FILE_IO.h"
#include "ace/Log_Msg.h"
#include "ace/Basic_Types.h"
#include "ace/CDR_Stream.h"
#include "ace/SString.h"


#include "Chat_FSM.h"

#include <cstdio>
#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

//extern hash_map< char*, char*> user_list;
//extern int send_response(ACE_SOCK_Stream &p, size_t type,size_t sub_t,char * detail);
extern vector<Chatroom> chatroom_list;

int Chat_FSM::run() {
    std::cout << "start running FSM "<<std::endl;
    for (;;) {
        if ( handle_data () != 0 ) return -1;
        std::cout << "current state of FSM: "<< curr_state_ <<std::endl;
    }
    
    return 0;
}



int Chat_FSM::handle_data() {

    char user_id_len;
    char user_id[16];
    char passwd_len;
    char passwd[16];
    char ip_addr[16];
    char r_len;
    char recipient[16];
    char s_len;
    char sender;
    char sha1[20];
    char message[msg_length-55];
    char poll_type;
    char ext_poll_len;
    char ext_poll[msg_length-18];
    char group_len;
    char group[16];

    std::cout << "start handle_data" << std::endl;
    //receive the msg length field
    if (this->peer_.recv_n( &msg_length,2) != 2) {
    //for debug
        std::cout << "error:message length is:" << msg_length << std::endl;
        return -1;
    }
        std::cout << "okay :message length is:" << msg_length << std::endl;

    //encapsualte the rest in an ace_message_block object
    ACE_Message_Block *payload = new ACE_Message_Block(ACE_DEFAULT_CDR_BUFSIZE);

    ACE_CDR::mb_align (payload);

    if (this->peer_.recv_n(payload->wr_ptr(),msg_length) != msg_length) return -1;

    payload->wr_ptr (msg_length);
    
    ACE_InputCDR cdr (payload);

    char msg_id;

    cdr >> msg_id;

    printf("message id is : %lu\n",(unsigned long) msg_id);

    switch (msg_id) {

        case 0 : //Handshake message 
        {
            if (curr_state_ != IDLE) return -1;

            char version[8];
            
            cdr.read_char_array(version,8);

            //version[7] = '\0';
            //std::cout << version << std::endl;

            ACE_CDR::Char sequence_no;
            cdr >> sequence_no;
            //printf("seq_no id is : %lu\n",(unsigned long) sequence_no);
            ACE_CDR::Char encryp_mode;
            cdr >> encryp_mode;
            //printf("encryp_mode is : %lu\n",(unsigned long) encryp_mode);
            size_t encry_payload_size;
            encry_payload_size = msg_length -11;
            char encry_payload[encry_payload_size];
            cdr.read_char_array(encry_payload,encry_payload_size);
            //encryption payload
            //encry_payload[encry_payload_size-1]='\0';
            //std::cout << encry_payload << std::endl;
            delete payload;

            //return success response
            if ( send_response ( peer_, 1 , (size_t) msg_id )
                    != 0) return -1;
            curr_state_ = CONNECTED;

            return 0;

        }//:case 0

        case 1://Register 
        {
            if (curr_state_ != CONNECTED) return -1;

            cdr >> user_id_len;
            cdr.read_char_array(user_id,16);
            cdr >> passwd_len;
            cdr.read_char_array(passwd,16);
            cdr.read_char_array(ip_addr,16);


            if ( user_list.find (user_id) == user_list.end() )
                user_list[user_id]  = passwd;
            else 
                if (strcmp(user_list[user_id],passwd) !=0) {
                    //send fail respond
                    if ( send_response ( peer_, 0 , (size_t) msg_id,
                                "another user_id already exists")
                        != 0) return -1;
                    return -1;
                }
            //send success respond 
            if ( send_response ( peer_, 1 , (size_t) msg_id )
                    != 0) return -1;

            return 0;

        }//: case 1

        case 2: //Authenticate
        {
            if (curr_state_ != CONNECTED) return -1;
            cdr >> user_id_len;
            cdr.read_char_array(user_id,16);
            cdr >> passwd_len;
            cdr.read_char_array(passwd,16);
            cdr.read_char_array(ip_addr,16);

            //check if user is authenticated, in this version, assume
            //everyone is authenticated

            //send success msg
            curr_state_ = LOBBY;

            return 0;
        }//: case 2

        case 3://Chat
        {
            if (curr_state_ != CHATROOM) return -1; 
            cdr >> r_len;
            cdr.read_char_array(recipient,16);
            cdr >> s_len;
            cdr.read_char_array(sender.16);
            cdr.read_char_array(sha1, 20);
            cdr.read_char_array(message,msg_length-55);
            
            return cr_->send_msg(CHAT,recipient,sender,message,msg_length-55);

        }

        case 4: //Quit
        {
           //quit 
           return 0;
        }

        case 5: //Block
        {
            if ( curr_state_ != CHATROOM ||
                    curr_state_ != MONITOR) return -1;
            cdr.read_char_array(user_id,16);

            cr_->block(user_id);

            if ( send_response ( peer_, 1 , (size_t) msg_id )
                    != 0) return -1;
            return 0;
        }

        case 6: //Unblock
        {
            if ( curr_state_ != CHATROOM ) return -1;
            cdr.read_char_array(user_id,16);

            cr_->unblock (user_id);

            return 0;
        }

        case 7: //Abbrev Poll
        {
            if ( curr_state_ != CHATROOM ) return -1;

            cdr >> poll_type;
            cdr >> user_id_len;
            cdr.read_char_array(user_id,16);

            return 0;

        }

        case 8://Extended Poll
        {
            if ( curr_state_ != CHATROOM ||
                 curr_state_ != MONITOR ) return -1;

            cdr >> ext_poll_len;
            cdr.read_char_array(ext_poll,msg_length-18);

            return 0;

        }

        case 9: //Create group
        {
            if (curr_state_ != LOBBY ||
                    curr_state_ != CHATROOM) return -1;

            cdr >> group_len;
            cdr.read_char_array(group,16);
            cr_ = new Chatroom ( user_id, peer_,group);
            chatroom_list[group] = cr_;
            return 0;
        }

        case 10: //Invite to goup

        {
            if ( curr_state_ != CHATROOM ||
                 curr_state_ != MONITOR ) return -1;

            cdr >> group_len;
            cdr.read_char_array(group,16);
            cdr >> user_id_len;
            cdr.read_char_array(user_id,16);

            return 0;

        }

        case 11: // Join group
        {
            if ( curr_state_ != LOBBY ) return -1;
            cdr >> group_len;
            cdr.read_char_array(group,16);
            
            if ( chatroom_list.find(group) == chatroom_list.end ()) {
                send_response(peer_, 0,11,"No chatroom found!");
                return -1;
            } else {
                cr_ = chatroom_list[group];
                send_response(peer_, 1,11,"You joined this chatroom!");
            return 0;
            }
        }

        case 12: //Leave group
        {
            cdr >> group_len;
            cdr.read_char_array(group,16);

            if ( user_list.find(user_id) != user_list.end ()) {
                user_list.erase(user_id);
                send_response(peer_, 1,12,"you left the chat room!");
            }

            return 0;
        }

        case 13://response
        {

        }

        default:
        {
            return -1; // should'nt get here
        }













    }//:switch


}//:Chat_FSM::handle_data








