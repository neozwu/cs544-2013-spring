#ifndef CHAT_FSM_H
#define CHAT_FSM_H
#include "ace/ACE.h"


#include "ace/SOCK_Stream.h"


enum states { IDLE = 0 ,CONNECTED = 1, LOBBY = 2, CHATROOM = 3, MONITOR=4 };
class ACE_SOCK_Stream;
class Chat_FSM {

protected:
    ACE_UINT16 msg_length; //unsigned int msg_length;
    ACE_SOCK_Stream peer_;
    int curr_state_;
    ACE_Message_Block * curr_msg;

public:
    Chat_FSM (ACE_SOCK_Stream &peer) 
        : peer_(peer),curr_state_ (IDLE),curr_msg(0),msg_length(0) {} 

    int run();  
    int handle_data ();

   // ~Chat_FSM () {}

private:
    Chatroom* cr_; //

};//:clas Chat_FSM

#endif



