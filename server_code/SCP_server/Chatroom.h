#ifndef CHATROOM_H
#define CHATROOM_H

#include <vector>
#include <algorithm>
class Chatroom {

static map<string, Chatroom* > chatroom_list;

public:
    Chatroom (char *creater, ACE_SOCK_Stream &peer,string rn= "chat");  
    ~Chatroom () {}
    int send_msg ( char *r, char *s, size_t msg_len);
    int block_user ( char *u );
    int unblock_user ( char *u );
    int join_chatroom ( char *u, char *p );

private:
    std::map<char *, char *> user_list;
    std::vector<char *> unblock_list;
    int room_status;
    char *creater_;
    ACE_SOCK_Stream peer_;
    string room_name_;
};
#endif
