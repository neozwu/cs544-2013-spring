#include "Chatroom.h"

#include <map>
#include <string>

extern map<string, Chatroom *> chatroom_list;
extern map<string, Chatroom *>::iterator chatroom_list_it;

Chatroom::Chatroom ( char *creater, ACE_SOCK_Stream &peer, string rn="chat")
    :creater_(creater),peer_(peer),room_name(rn) {
        chatroom_list[rn] = this;
}

    
    
int Chatroom::send_msg ( size_t subtype, char *r, char *s, char *m, size_t msg_len) {

    size_t r_size, s_size;
    r_size = strlen (r) + 1;
    s_size = strlen (s) + 1;
/*
    ACE_InputCDR cdr;
    ACE_Message_Block *chat_msg = new ACE_Message_Block(ACE_DEFAULT_CDR_BUFSIZE);
    ACE_CDR::mb_align (chat_msg);

    cdr.write_char_array(r,r_size);
    cdr.write_char_array(s,s_size);
    cdr.write_char_array(m, msg_len);
    */

    size_t m_id = 13;
    size_t m_t = 1;
    size_t s_t = subtype;
    iovec iov[7];
    ACE_UInt16 total_len;
    total_len = msg_len + 2 +3;
    iov[0].io_base = total_len; //the msg length
    iov[0].io_len = 2;
    iov[1].io_base = m_id; //msg id, 13
    iov[1].io_len = 1;
    iov[2].io_base = m_t; // type field in the response message
    iov[2].io_len = 1;
    iov[3].io_base = s_t; // subtype field in the response message
    iov[3].io_len = 1;
    iov[4].io_base = r; // the recipient of the chat message
    iov[4].io_len = 1;
    iov[5].io_base = s; // the sender of the chat message
    iov[5].io_len = 1;
    iov[6].io_base = m;
    iov[6].io_len = msg_len;  // the 'message' in the response message body

    //peer_.sendv_n(iov,7);
    //TO DO:
    //I can't fine detailed spec in the protocol as to how the server
    //broadcasts the chat message to all unblocked users in the chat room.
    //
    //Implemetation-wise, it can be implemented as a global inbox. So eachtime, 
    //an FSM should query the inbox to see if there's any unread chat message
    //for a particular user.
    //

    return 0;

}

int Chatroom::block ( char *u) {
    if ( user_list.find ( u ) == user_list.end () ) return -1;
    if ( unblock_list.find ( u ) != unblock_list.end () )  
        unblock_list.putback ( u );
    return 0;

}


int Chatroom::unblock (char *u ) {
    if ( unblock_list.find (u) == unblock_list.end() )
        unblock_list.put_back (u);

    return 0;
}

int Chatroom::join_chatroom ( char *u, char *p )  {
    if ( user_list.find (u) == user_list.end ()) {
        user_list[u] = p;
        return 0;
    }
    else 
        return -1;
}

