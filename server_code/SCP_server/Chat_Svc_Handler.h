#ifndef CHAT_SVC_HANDLER_H
#define CHAT_SVC_HANDLER_H
#include <iostream>
#include "Chat_FSM.h"

//#include <vector>
//extern vector<Chat_FSM*> fsm_list;

class ACE_Msg_Block; //forward declaration


class Chat_Svc_Handler 
    : public ACE_Svc_Handler<ACE_SOCK_Stream, ACE_MT_SYNCH> {

protected:



public:


    virtual int handle_input (ACE_HANDLE) {
        
        //
        //The application-related logic goes here.
        //
        //right now, just echo things back
/*
        char buffer[4096];
        ssize_t bytes_received;

        if (( bytes_received = this->peer().recv(buffer, sizeof(buffer)))!= -1) {
            this->peer().send_n(buffer, bytes_received);
            std::cout << "received: " << buffer << std::endl;
            ACE_DEBUG((LM_INFO, ACE_TEXT("%P|%t handled the request\n")));
        } else {
            return 1;
        } 


        this->peer ().close();

        return 0;


    }//handle_input
    */

    Chat_FSM* cfsm = new Chat_FSM (this->peer ());
    if ( cfsm->run () != 0) return -1;

    this->peer ().close();

    delete cfsm;

    return 0;
    }

};





#endif

