#include "ace/ACE.h"
#include "ace/FILE_Addr.h"
#include "ace/Sock_Stream.h"
#include "ace/FILE_IO.h"
#include "ace/Log_Msg.h"
#include "ace/Basic_Types.h"
#include "ace/CDR_Stream.h"
#include "ace/SString.h"


#include <string>

//send a response message to all unblocked users in the chat room.
int send_response(ACE_SOCK_Stream &p, size_t type,size_t sub_t,char * detail="") {
    iovec iov[4];
    size_t m_id = 13;
    size_t m_t = type;
    size_t s_t = sub_t;
    size_t d_len = sizeof(detail);

    iov[0].iov_base = &m_id;
    iov[0].iov_len = 1;

    iov[1].iov_base = &m_t;
    iov[1].iov_len = 1;

    iov[2].iov_base = &s_t; 
    iov[2].iov_len = 1;

    iov[3].iov_base = detail;
    iov[3].iov_len = d_len;

    return p.sendv_n ( iov, 4);

}

