#ifndef QUIT_HANDLER_H
#define QUIT_HANDLER_H
#include <ace/Event_Handler.h>

class Quit_Handler : public ACE_Event_Handler {
public:
  Quit_Handler (ACE_Reactor *r) : ACE_Event_Handler (r) {}

  virtual int handle_exception (ACE_HANDLE) {
    reactor ()->end_reactor_event_loop ();
    return -1; // Trigger call to handle_close() method.
  }

  virtual int handle_close (ACE_HANDLE, ACE_Reactor_Mask)
  { delete this; return 0; }

protected:
  virtual ~Quit_Handler () {}
};

#endif
